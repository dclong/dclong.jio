# README #

The binary data format (`readBin` and `writeBin`) in R is not compatible with Java. This package helps read and write binary data that are compatible with Java. 