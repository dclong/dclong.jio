#' @title Read and Write Binary Data 
#' @description The following functions read and write binary data that is compatible with classes 
#' DataOutputStream and DataInputStream in Java.
#' @param data the data to be written into a file.
#' @param file the file into which the data is written to or from which the data is read from.
#' @param length the number of data to read in.
#' @param first the 0-based-byte index of the first data to be read in. 
#' @param skip the number of bytes to skip each time after reading a data.
#' This argument together with \code{first} allows flexible ways to read in data.

#' @export
#' @rdname io
#' @importFrom rJava .jarray .jcall
writeDouble = function(data,file){
    rJava::.jcall('dclong/io/BinaryWriter',returnSig='V',method='writeDouble',rJava::.jarray(data),file)
}
#' @export
#' @rdname io
writeFloat = function(data,file){
    rJava::.jcall('dclong/io/BinaryWriter',returnSig='V',method='writeFloat',rJava::.jarray(data),file)
}
#' @export
#' @rdname io
writeInt = function(data,file){
    rJava::.jcall('dclong/io/BinaryWriter',returnSig='V',method='writeInt',rJava::.jarray(data),file)
}
#' @export
#' @rdname io
writeBoolean = function(data,file){
    rJava::.jcall('dclong/io/BinaryWriter',returnSig='V',method='writeBoolean',rJava::.jarray(data),file)
}
#' @export
#' @rdname io
readDouble = function(file,length,first,skip){
    if(missing(length)){
        return(rJava::.jcall('dclong/io/BinaryReader',returnSig="[D", method='readDouble',file))
    }
    if(missing(first)){
        first = 0
    }
    if(missing(skip)){
        skip = 0
    }
    rJava::.jcall('dclong/io/BinaryReader',returnSig="[D",
                         method='readDouble',file,as.integer(first),as.integer(skip),as.integer(length))
}
#' @export
#' @rdname io
readFloat = function(file,length,first,skip){
    if(missing(length)){
        return(rJava::.jcall('dclong/io/BinaryReader',returnSig="[F", method='readFloat',file))
    }
    if(missing(first)){
        first = 0
    }
    if(missing(skip)){
        skip = 0
    }
    rJava::.jcall('dclong/io/BinaryReader',returnSig="[F",
                         method='readFloat',file,as.integer(first),as.integer(skip),as.integer(length))
}
#' @export
#' @rdname io
readInt = function(file,length,first,skip){
    if(missing(length)){
        return(rJava::.jcall('dclong/io/BinaryReader',returnSig="[I", method='readInt',file))
    }
    if(missing(first)){
        first = 0
    }
    if(missing(skip)){
        skip = 0
    }
    rJava::.jcall('dclong/io/BinaryReader',returnSig="[I",
                         method='readInt',file,as.integer(first),as.integer(skip),as.integer(length))
}
#' @export
#' @rdname io
readBoolean = function(file,length,first,skip){
    if(missing(length)){
        return(rJava::.jcall('dclong/io/BinaryReader',returnSig="[Z", method='readBoolean',file))
    }
    if(missing(first)){
        first = 0
    }
    if(missing(skip)){
        skip = 0
    }
    rJava::.jcall('dclong/io/BinaryReader',returnSig="[Z",
                         method='readBoolean',file,as.integer(first),as.integer(skip),as.integer(length))
}
